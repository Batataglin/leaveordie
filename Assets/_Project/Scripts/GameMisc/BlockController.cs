﻿using UnityEngine;

public class BlockController : MonoBehaviour
{
    private void Awake()
    {
        GetComponent<DamageController>().OnDestroy += DestroyHandle;
    }

    private void DestroyHandle()
    {
        Destroy(gameObject);
    }
}

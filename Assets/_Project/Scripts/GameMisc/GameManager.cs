﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
 
    private GameManager() { }

    [Header("Scenes")]
    [SerializeField] private string _resultScene = "MatchResult";

    private Winner _matchWinner;
    
    public static GameManager Instance => _instance;

    public Winner MatchWinner => _matchWinner;
    
    public GameStatus GameState
    {
        get;
        set;
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    
    public void HandleMatchFinish(Winner winner)
    {
        _matchWinner = winner;   
        
        SceneManager.LoadScene(_resultScene);
    }

    public void HandleChangeScene(string loadScene)
    {
        SceneManager.LoadScene(loadScene);
    }
}

﻿using UnityEngine;

public class MapLimits : MonoBehaviour
{
    [SerializeField] private int _worldWidth = 50;
    [SerializeField] private int _worldHeight = 40;

    private void Start()
    {
        GameObject barrier = new GameObject("LimitBarrier");
        
        var instance = Instantiate(barrier, new Vector2(0,-_worldHeight / 2), Quaternion.identity);

        instance.transform.SetParent(transform);
        instance.AddComponent<BoxCollider2D>().size = new Vector2(_worldWidth, 1);
        
        for (int i = -1; i < 3; i += 2)
        {
            Vector2 barrierPosition = new Vector2((_worldWidth / 2) * i, 0);

            instance = Instantiate(barrier, barrierPosition, Quaternion.identity);

            instance.transform.SetParent(transform);
            instance.AddComponent<BoxCollider2D>().size = new Vector2(1, _worldHeight);
        }
    }
}

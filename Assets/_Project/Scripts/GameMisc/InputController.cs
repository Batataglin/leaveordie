﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    public delegate void DirectionHandler(Vector2 direction);
    public delegate void FlipSpriteHandler(Vector2 direction);
    public delegate void SimpleActionHandler();

    public event DirectionHandler OnMovementAction;
    public event DirectionHandler AimAction;
    public event FlipSpriteHandler FlipSprite;
    public event SimpleActionHandler OnJumpAction;
    public event SimpleActionHandler OnShotAction;

    [SerializeField] private PlayerInput _input;

    private void Update()
    {
        if (_input.IsJoystick)
        {
            var horizontal = Input.GetAxisRaw(_input.Horizontal);
            var vertical = Input.GetAxisRaw(_input.Vertical);

            if (horizontal < 0)
            {
                VerticalMovement(Vector2.left);
            }
            else if (horizontal > 0)
            {
                VerticalMovement(Vector2.right);
            }

            if (vertical > 0)
            {
                HorizontalMovement(Vector2.up);
            }
            else if (vertical < 0)
            {
                HorizontalMovement(Vector2.down);
            }

            if (Input.GetButton(_input.JoyJetpack))
            {
                OnJumpAction?.Invoke();
            }

            if (Input.GetButton(_input.JoyShoot))
            {
                OnShotAction?.Invoke();
            }
        }
        else
        {
            if (Input.GetKey(_input.Left))
            {
                VerticalMovement(Vector2.left);
            }
            else if (Input.GetKey(_input.Right))
            {
                VerticalMovement(Vector2.right);
            }

            if (Input.GetKey(_input.Up))
            {
                HorizontalMovement(Vector2.up);
            }
            else if (Input.GetKey(_input.Down))
            {
                HorizontalMovement(Vector2.down);
            }

            if (Input.GetKey(_input.Jetpack))
            {
                OnJumpAction?.Invoke();
            }

            if (Input.GetKey(_input.Shoot))
            {
                OnShotAction?.Invoke();
            }
        }
    }

    private void VerticalMovement(Vector2 direction)
    {
        FlipSprite?.Invoke(direction);
        OnMovementAction?.Invoke(direction);
        AimAction?.Invoke(direction);
    }

    private void HorizontalMovement(Vector2 direction)
    {
        AimAction?.Invoke(direction);
    }
    
    
    
}

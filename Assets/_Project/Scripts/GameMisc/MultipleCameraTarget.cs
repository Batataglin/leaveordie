﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MultipleCameraTarget : MonoBehaviour
{
    [SerializeField] private float _minimumZoom = 40f;
    [SerializeField] private float _maximumZoom = 10f;
    [SerializeField] private float _zoomLimiter = 10f;
    
    private readonly Vector3 _offset = new Vector3(0,0,-10f);
    public static List<Transform> CameraTargets { get; set; }

    private float _smoothTime = 0.5f;
    private Camera _camera;
    private Vector3 _velocity;
    
    private void Awake()
    {
        _camera = GetComponent<Camera>();
        CameraTargets = new List<Transform>();
    }

    private void LateUpdate()
    {
        if (CameraTargets.Count == 0)
        {
            return;
        }
        
        CameraMovement();
        CameraZoom();
    }

    private void CameraZoom()
    {
        Bounds bounds = GetBounds();

        float newZoom = Mathf.Lerp(_maximumZoom, _minimumZoom, bounds.size.x / _zoomLimiter);
        _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, newZoom, Time.deltaTime * 2f);
    }
    
    
    private void CameraMovement()
    {
        Vector3 centerPoint = GetCenterPoint();
        Vector3 newPosition = centerPoint + _offset;

        transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref _velocity, _smoothTime);
    }

    private Bounds GetBounds()
    {
        Bounds bounds = new Bounds(CameraTargets[0].position,Vector3.zero);

        for (int i = 0; i < CameraTargets.Count; i++)
        {
            bounds.Encapsulate(CameraTargets[i].position);
        }

        return bounds;
    }

    private Vector3 GetCenterPoint()
    {
        if (CameraTargets.Count == 1)
        {
            return CameraTargets[0].position;
        }

        Bounds bounds = GetBounds();

        return bounds.center;
    }
}

﻿public enum GameLayer
{
    Player,
    PlayerSpawnShield,
    Item,
    Ground,
    Gameplay
}
public enum GameStatus
{
    INGAME,
    MAINMENU,
    PAUSEMENU,
    ENDMATCH
}

public enum InterationCode
{
    Collect = 0,
    Install = 1,
    Remove = 2
}

public enum Winner
{
    None = 0,
    PlayerOne = 1,
    PlayerTwo = 2
}

public enum Scene
{
    VersusGame,
    MultiplayerGame,
    MatchResults,
    Main
}
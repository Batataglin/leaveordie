﻿using UnityEngine;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(Rigidbody2D))]
public class LaserBlast : MonoBehaviour
{
    [SerializeField] private float _laserSpeed = 500f;

    private bool _hasHitSomething;
    private float _lifeTime = 3f;
    private AudioSource _audioSource;
    private LayerMask _groundLayer;
    private LayerMask _playerLayer;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();

        _groundLayer = LayerMask.NameToLayer(GameLayer.Ground.ToString());
        _playerLayer = LayerMask.NameToLayer(GameLayer.Player.ToString());
    }

    public void Shot(Vector2 direction)
    {
        _audioSource.Play();
        GetComponent<Rigidbody2D>().AddForce(direction * _laserSpeed);
        Destroy(gameObject, _lifeTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(_hasHitSomething == true)
        {
            return;
        }
        
        var otherLayer = other.gameObject.layer;

        if (otherLayer == _groundLayer)
        {
            RaycastHit2D hit = Physics2D.Linecast(transform.position, GetComponent<Rigidbody2D>().velocity.normalized);

            Tilemap tilemap = FindObjectOfType<Tilemap>();
            tilemap.SetTile(tilemap.WorldToCell(hit.point), null);
            _hasHitSomething = true;
        }
        else if (otherLayer == _playerLayer)
        {
            IDamageable damageable = other.GetComponent<IDamageable>();

            if (damageable != null)
            {
                damageable.ApplyDamage(1);
                _hasHitSomething = true;
            }
            else
            {
                print("Damageable is null");
            }
        }
        
        Destroy(gameObject);
    }
}

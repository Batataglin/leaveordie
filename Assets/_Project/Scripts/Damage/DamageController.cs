﻿using UnityEngine;

public class DamageController : MonoBehaviour, IDamageable
{
    public delegate void RecieveDamageHandler(int currentLife);
    public delegate void OnDieHandler();

    public event RecieveDamageHandler OnRecieveDamage;
    public event OnDieHandler OnDestroy;
    
    [SerializeField] private int _maximumDamage = 1;

    public int CurrentLife { get; private set; }

    private void Start()
    {
        CurrentLife = _maximumDamage;
    }
    
    private void RecieveDamage(int damage)
    {
        CurrentLife -= damage;
        
        if (CurrentLife <= 0f)
        {
            CurrentLife = 0;
            
            if (OnDestroy != null)
            {
                OnDestroy.Invoke();
            }
        }
        
        if (OnRecieveDamage != null)
        {
            OnRecieveDamage.Invoke(CurrentLife);
        }
    }

    void IDamageable.ApplyDamage(int damage)
    {
        RecieveDamage(damage);
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour
{
    private int _spaceshipId;

    public int SpaceshipId => _spaceshipId;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.gameObject.GetComponent<PlayerController>();

        if (player != null)
        {
            player.HandleTriggerEnterInteraction(gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var player = other.GetComponent<PlayerController>();

        if (player != null)
        {
            player.HandleTriggerExitInteraction();
        }
    }
    
    public void SetSpaceshipId(int id)
    {
        _spaceshipId = id;
    }
}
   
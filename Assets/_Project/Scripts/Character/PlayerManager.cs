﻿using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private PlayerController _playerController;
    private MovementController _movementController;
    private InputController _inputController;
    private DamageController _damageController;
    private AimController _aimController;
    private Interaction _interaction;
    private MatchManager _matchManager;

    private void Awake()
    {
        _matchManager = FindObjectOfType<MatchManager>();
        
        _movementController = GetComponent<MovementController>();
        _inputController = GetComponent<InputController>();
        _playerController = GetComponent<PlayerController>();
        _damageController = GetComponent<DamageController>();
        _aimController = GetComponent<AimController>();
        _interaction = GetComponent<Interaction>();
        
        _inputController.OnMovementAction += _movementController.Move;
        _inputController.OnJumpAction += _movementController.Jump;
        _inputController.FlipSprite += _playerController.Flip;
        _inputController.AimAction += _aimController.AimDirection;
        _inputController.OnShotAction += _aimController.OnShot;

        _damageController.OnDestroy += _playerController.HandleOnDie;
        _damageController.OnDestroy += _playerController.HandlerSpawnItems;

        _playerController.OnMovementStateChange += _movementController.HandleMovementStateChange;
        _playerController.OnMovementStateChange += _aimController.HandleMovementStateChange;
        
        _playerController.OnStartInteract += _interaction.HandleStartInteraction;
        _playerController.OnExitInteract += _interaction.HandleExitInteraction;

        _playerController.IsNeeded += _matchManager.HandleIsPartNeeded;
        _playerController.IsCollected += _matchManager.HandleCollectPart;
        _playerController.OnDrop += _matchManager.HandleDropPart;
        _playerController.OnRefreshCheckList += _matchManager.HandleDropCollectedItens;
        _playerController.OnReadyToFlight += _matchManager.HandleShipReadyToFlight;
        _playerController.IsAllPartCollected += _matchManager.HandlesIsAllPartCollected;
        _playerController.OnGetCheckList += _matchManager.HandleGetCheckList;
        
        _interaction.IsPartNeeded += _playerController.HandleIsPartNeeded;
        _interaction.IsAllPartCollected += _playerController.HandleAllPartsCollected;
        _interaction.OnCollectPart += _playerController.HandleCollectSpaceshipPart;
        _interaction.ReadytoFlight += _playerController.HandleReadyToFlight;

        _matchManager.OnBegin += _playerController.HandleBegin;
        
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [Header("Jetpack")]
    [SerializeField] private float _jetpackForce = 150f;
    [SerializeField] private float _jetpackStamina = 50f;
    [SerializeField] private float _jetpackStaminaUsageFactor = 3f;
    [SerializeField] private float _jetpackStaminaRecoverFactor = 2f;
    
    [Space]  
    [SerializeField] private float _movementSpeed = 5f;

    [Range(0, .3f)] [SerializeField] private float _movementSmoothing = .05f;

    private bool _canMove;
    private float _currentJetpackStamina;
    private Rigidbody2D _rigidbody2D;
    private Vector3 _velocity = Vector3.zero;
    
    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        StartCoroutine(JetpackCoroutine());
    }
    
    public void HandleMovementStateChange(bool state)
    {
        _canMove = state;
    }

    public void Move(Vector2 direction)
    {
        if (_canMove == false)
        {
            return;
        }
    
        var velocity = _rigidbody2D.velocity;
        Vector3 targetVelocity = new Vector2(_movementSpeed * direction.x, velocity.y);
        _rigidbody2D.velocity = Vector3.SmoothDamp(velocity, targetVelocity, ref _velocity, _movementSmoothing);
    }

    public void Jump()
    {
        if (_canMove == false)
        {
            return;
        }

        if (_currentJetpackStamina <= 0f)
        {
            return;
        }
        
        _rigidbody2D.AddForce(new Vector2(0f, _jetpackForce),ForceMode2D.Force);
        _currentJetpackStamina -= _jetpackStaminaUsageFactor * Time.fixedDeltaTime;
    }

    private IEnumerator JetpackCoroutine()
    {
        while (true)
        {
            if (_currentJetpackStamina < _jetpackStamina)
            {
                _currentJetpackStamina += _jetpackStaminaRecoverFactor * Time.fixedDeltaTime;
            }
            
            yield return null;
        }
    }
}

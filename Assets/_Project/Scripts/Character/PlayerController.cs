﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlayerController : MonoBehaviour
{
    private enum SpawnLookSide
    { Right, Left}

    public delegate void InteractionHandler(GameObject interactGameObject);
    public delegate void StopInteractionHandler();
    public delegate void MovementStateHandler(bool state);
    public delegate bool PartNeededHandler(SpaceshipPart spaceshipPart, int index);
    public delegate void PartCollectedHandler(int playerIndex, SpaceshipPart dictionaryKey, InterationCode interactionCode);
    public delegate void ShipReadyToFlightHandler(Winner winner);
    public delegate bool IsAllPartCollectedHandler(int listIndex);
    public delegate void RefreshCheckListHandler(int listIndex);
    public delegate Dictionary<SpaceshipPart, bool> GetCheckListHandler(int listIndex);
    
    public event RefreshCheckListHandler OnRefreshCheckList;
    public event GetCheckListHandler OnGetCheckList;
    public event IsAllPartCollectedHandler IsAllPartCollected;
    public event MovementStateHandler OnMovementStateChange;
    public event InteractionHandler OnStartInteract;
    public event StopInteractionHandler OnExitInteract;
    public event PartNeededHandler IsNeeded;
    public event PartCollectedHandler IsCollected;
    public event PartCollectedHandler OnDrop;
    public event ShipReadyToFlightHandler OnReadyToFlight;

    [SerializeField] private GameObject _itemPrefab;
    
    [SerializeField] private Winner _playerValue;
    [SerializeField] private SpawnLookSide _spawnLook = SpawnLookSide.Right;
    [SerializeField] private float _respawnTime = 3f;
    [SerializeField] private Transform _spriteContainer;

    [Header("Shield")]
    [SerializeField] private GameObject _shield;
    [SerializeField] private float _protectionTime = 2f;

    private int _playerId;
    private int _playerListItemIndex;
    private Vector2 m_previousDirection;
    private Coroutine _respawnCoroutine;
    private Rigidbody2D _rigidbody2D;
    private List<Collider2D> _colliders = new List<Collider2D>();
    private List<SpriteRenderer> _sprites = new List<SpriteRenderer>(); 
    
    public int PlayerId => _playerId;
    public int PlayerListItemIndex => _playerListItemIndex;
    
    private void Awake()
    {
        _playerId = Random.Range(1111, 9999);
    }

    public void Start()
    {
        OnMovementStateChange?.Invoke(false);
        gameObject.name = string.Format("Player {0}", _playerListItemIndex + 1);
        gameObject.layer = LayerMask.NameToLayer(GameLayer.Player.ToString());

        if (_spawnLook == SpawnLookSide.Left)
        {
            Flip(Vector2.left);
        }
        else
        {
            Flip(Vector2.right);
        }
        
        GetComponents();
        Spawn();
    }
    
    private void GetComponents()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        
        var colliders = GetComponents<Collider2D>();
        foreach (var collider in colliders)
        {
            _colliders.Add(collider);
        }
        
        colliders = GetComponentsInChildren<Collider2D>();
        foreach (var collider in colliders)
        {
            _colliders.Add(collider);
        }

        var sprites = GetComponents<SpriteRenderer>();
        foreach (var sprite in sprites)
        {
            _sprites.Add(sprite);
        }
        
        sprites = GetComponentsInChildren<SpriteRenderer>();
        foreach (var sprite in sprites)
        {
            _sprites.Add(sprite);
        }
    }

    public void HandleReadyToFlight()
    {
        OnReadyToFlight?.Invoke(_playerValue);
    }
    
    public void HandleCollectSpaceshipPart(SpaceshipPart spaceshipPart)
    {
        if (IsCollected == null)
        {
            Debug.LogError("Call for is collected is empty");
            return;
        }
        
        IsCollected.Invoke(_playerListItemIndex,spaceshipPart,InterationCode.Collect);
    }

    public bool HandleIsPartNeeded(SpaceshipPart spaceshipPart)
    {
        if (IsNeeded == null)
        {
            Debug.LogError("Call for part needed is empty");
            return false;
        }
        
        return IsNeeded.Invoke(spaceshipPart, _playerListItemIndex);
    }

    public void HandleTriggerEnterInteraction(GameObject interactGameObject)
    {
        OnStartInteract?.Invoke(interactGameObject);
    }
    
    public void HandleTriggerExitInteraction()
    {
        OnExitInteract?.Invoke();
    }

    public bool HandleAllPartsCollected()
    {
        if (IsAllPartCollected == null)
        {
            return false;
        }
        
        return IsAllPartCollected.Invoke(_playerListItemIndex);
    }

    public void HandleBegin()
    {
        OnMovementStateChange?.Invoke(true);
    }

    private bool _spawning = false;

    public void HandlerSpawnItems()
    {
        if (_spawning == true)
            return;

        _spawning = true;
        
        float spawnRange = 5f;

        Dictionary<SpaceshipPart, bool> inventoryList = OnGetCheckList?.Invoke(_playerListItemIndex);

        if (inventoryList == null)
        {
            Debug.LogError("SpawnItems check list is empty");
            return;
        }
        
        foreach (var part in inventoryList)
        {
            SpaceshipPart item = part.Key;
            
            if (inventoryList[item] == true)
            {
                Vector3 position = Random.insideUnitCircle * spawnRange;
                
                var instance = Instantiate(_itemPrefab,transform.position +  position, Quaternion.identity);
                instance.GetComponent<CollectablePart>().Initialize(item);

                OnDrop.Invoke(_playerListItemIndex, item, InterationCode.Remove);
            }
        }
        
        OnRefreshCheckList?.Invoke(_playerListItemIndex);
        
        _spawning = false;
    }

    public void Flip(Vector2 side)
    {
        if (m_previousDirection == side)
        {
            return;
        }
        
        m_previousDirection = side;
        
        Vector3 theScale = _spriteContainer.localScale;
        theScale.x *= -1;
        _spriteContainer.localScale = theScale;
    }

    public void HandleOnDie()
    {
        OnMovementStateChange?.Invoke(false);
        PlayerElementsHandler(false);
        
        if (_respawnCoroutine == null)
        {
            _respawnCoroutine = StartCoroutine(RespawnCoroutine());
        }
    }

    public void SetPlayerListItemIndex(int index)
    {
        _playerListItemIndex = index;
    }
    
    private IEnumerator RespawnCoroutine()
    { 
        yield return new WaitForSeconds(_respawnTime);

        transform.position = transform.parent.position;

        Spawn();
        _respawnCoroutine = null;
        OnMovementStateChange?.Invoke(true);
    }

    private IEnumerator ShieldCoroutine()
    {
        var timer = _protectionTime;
        _shield.SetActive(true);
        
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        
        _shield.SetActive(false);
    }

    private void Spawn()
    {
        PlayerElementsHandler(true);

        StartCoroutine(ShieldCoroutine());
    }
    
    private void PlayerElementsHandler(bool state)
    {
        transform.localRotation = Quaternion.identity;
        _rigidbody2D.simulated = state;
        
        foreach (var sprite in _sprites)
        {
            sprite.enabled = state;
        }

        foreach (var collider in _colliders)
        {
            collider.enabled = state;
        }
    }
}

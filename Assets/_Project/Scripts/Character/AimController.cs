﻿using UnityEngine;

public class AimController : MonoBehaviour
{
    [Header("Weapon")]
    [SerializeField] private GameObject _laserBlast;
    [SerializeField] private Transform _shotPosition;
    [SerializeField] private Transform _weaponPivot;
    [SerializeField] private float _weaponCadence = 0.3f;

    private bool _canMove;
    private float _timer;
    private Vector2 _previousDirection;

    private void Update()
    {
        _timer += Time.fixedDeltaTime;
    }
    
    public void HandleMovementStateChange(bool state)
    {
        _canMove = state;
    }
    
    public void AimDirection(Vector2 direction)
    {
        if (_previousDirection == direction)
        {
            return;
        }

        _previousDirection = direction;

        Vector2 localRotation = direction * 90;
        
        _weaponPivot.localEulerAngles = new Vector3(0,0,localRotation.y);
    }

    public void OnShot()
    {
        if (_canMove == false)
        {
            return;
        }

        if (_timer > _weaponCadence)
        {
            var instance = Instantiate(_laserBlast, _shotPosition.position, _shotPosition.rotation);
            instance.GetComponent<LaserBlast>().Shot(_previousDirection);
            _timer = 0;
        }
    }
}

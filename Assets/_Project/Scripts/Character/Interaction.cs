﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Interaction : MonoBehaviour
{
    public delegate bool IsAllPartCollectedHandler();
    public delegate void CollectionSuccess(SpaceshipPart spaceshipPart);
    public delegate bool SpaceshipPartIsNeededHandler(SpaceshipPart spaceShipPart);
    public delegate void SpaceshipReadyToFlightHandler();

    public event IsAllPartCollectedHandler IsAllPartCollected;
    public event CollectionSuccess OnCollectPart;
    public event SpaceshipPartIsNeededHandler IsPartNeeded;
    public event SpaceshipReadyToFlightHandler ReadytoFlight;

    
    [SerializeField] private Sprite _collectingPart;
    [SerializeField] private Image _progressBar;
    
    private Coroutine _installSpaceshipPartCoroutine;
    private Coroutine _removeSpaceshipPartCoroutine;
    private Coroutine _collectSpaceshipPartCoroutine;
    private Coroutine _interactTimeCoroutine;

    private void Start()
    {
        InteractionUiStatus(false);
        _progressBar.fillAmount = 0f;
    }
    
    public void HandleStartInteraction(GameObject interactionGameObject)
    {
        ICollectable isCollectable = interactionGameObject.GetComponent<ICollectable>();

        if (isCollectable == null)
        {
            Spaceship spaceship = interactionGameObject.GetComponent<Spaceship>();

            if (spaceship != null)
            {
                if (spaceship.SpaceshipId == GetComponent<PlayerController>().PlayerId)
                {
                    if (IsAllPartCollected == null)
                    {
                        return;
                    }

                    if (IsAllPartCollected.Invoke())
                    {
                        if (_installSpaceshipPartCoroutine == null)
                        {
                            _installSpaceshipPartCoroutine = StartCoroutine(InstallSpaceshipPartCoroutine());
                        }
                    }
                }
            }
        }
        else
        {
            SpaceshipPart spaceshipPart = interactionGameObject.GetComponent<CollectablePart>().SpaceshipPart;

            if (IsPartNeeded != null)
            {
                if (IsPartNeeded.Invoke(spaceshipPart))
                {
                    if (_collectSpaceshipPartCoroutine == null)
                    {
                        _collectSpaceshipPartCoroutine = StartCoroutine(CollectSpaceshipPartCoroutine(spaceshipPart.TimeToInteract, spaceshipPart, isCollectable));
                    }
                }
            }
        }
    }
    
    public void HandleExitInteraction()
    {
        _progressBar.DOKill(false);
        InteractionUiStatus(false);
        
        if (_interactTimeCoroutine != null)
        {
            StopCoroutine(_interactTimeCoroutine);
            _interactTimeCoroutine = null;
        }
        
        if (_removeSpaceshipPartCoroutine != null)
        {
            StopCoroutine(_removeSpaceshipPartCoroutine);
            _removeSpaceshipPartCoroutine = null;
        }
        
        if (_installSpaceshipPartCoroutine != null)
        {
            StopCoroutine(_installSpaceshipPartCoroutine);
            _installSpaceshipPartCoroutine = null;
        }
       
        if (_collectSpaceshipPartCoroutine != null)
        {
            StopCoroutine(_collectSpaceshipPartCoroutine);
            _collectSpaceshipPartCoroutine = null;
        }
    }

    private IEnumerator InstallSpaceshipPartCoroutine()
    {
        yield return InteractTimeCoroutine(6f, 0, 1);
        
        ReadytoFlight?.Invoke();
    }

    private IEnumerator CollectSpaceshipPartCoroutine(float timeToCollect, SpaceshipPart spaceshipPart, ICollectable collectable)
    {
        _progressBar.sprite = _collectingPart;
        yield return _interactTimeCoroutine = StartCoroutine(InteractTimeCoroutine(timeToCollect, 0 ,1));
        OnCollectPart?.Invoke(spaceshipPart);
        _collectSpaceshipPartCoroutine = null;
        collectable.Collected();
    }
    
   private IEnumerator InteractTimeCoroutine(float timeToDoAction,int initialFillAmount, int finalFillAmount)
    {
        _progressBar.fillAmount = initialFillAmount;
        InteractionUiStatus(true);
        _progressBar.DOFillAmount(finalFillAmount, timeToDoAction);

        float time = 0f;
        
        while (time < timeToDoAction)
        {
            time += Time.fixedDeltaTime;
            
            yield return null;
        }
        
        InteractionUiStatus(false);

        _interactTimeCoroutine = null;
    }

    private void InteractionUiStatus(bool isActive)
    {
        if (_progressBar != null)
        {
            _progressBar.enabled = isActive;
        }
    }
}

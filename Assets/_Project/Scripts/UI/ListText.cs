﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ListText : MonoBehaviour
{
    [SerializeField] private Image _checkBox;
    [SerializeField] private Sprite _uncollectedCheckBox;
    [SerializeField] private Sprite _inventoryCheckBox;
    [SerializeField] private Sprite _installedCheckBox;
    [SerializeField] private TextMeshProUGUI _text;
    
    public void SetText(string text)
    {
        _text.text = text;
    }

    public void Uncollected()
    {
        _checkBox.sprite = _uncollectedCheckBox;
    }

    public void Collected()
    {
        _checkBox.sprite = _inventoryCheckBox;
    }
    
    public void Installed()
    {
        _checkBox.sprite = _installedCheckBox;
    }

    public void EnableObject(bool isActive)
    {
        gameObject.SetActive(isActive);
    }
    
}

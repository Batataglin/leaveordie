﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [Header("Game HUD")] 
    [SerializeField] private TextMeshProUGUI _timeText;
    [SerializeField] private TextMeshProUGUI _countdownTimeText;
    [SerializeField] private Image _darkTransparency;
    
    [Header("Players List")]
    [SerializeField] private GameObject _playerOneListItemTemplate;
    [SerializeField] private GameObject _playerTwoListItemTemplate;


    private List<Dictionary<SpaceshipPart, GameObject>> _playerList = new List<Dictionary<SpaceshipPart, GameObject>>(2);
    
#if UNITY_EDITOR
    private void Start()
    {
        _darkTransparency.gameObject.SetActive(false);
    }
#endif
    
    public void HandleMatchTimeUpdate(string time)
    {
        _timeText.text = time;
    }
    
    public void HandleCountdownUpdate(string time)
    {
        _countdownTimeText.text = time;
    }

    public void HandleMatchBegin()
    {
        _darkTransparency.enabled = false;
        _countdownTimeText.gameObject.SetActive(false);
    }

    public void CreateList(List<Dictionary<SpaceshipPart, bool>> checkList)
    {
        for (int i = 0; i < checkList.Count; i++)
        {
            Dictionary<SpaceshipPart, GameObject> list = new Dictionary<SpaceshipPart, GameObject>();

            foreach (var dictionaryList in checkList[i])
            {
                GameObject instance = null;
                ListText instaceText = null;
                
                if (i == 0)
                {
                    instance = Instantiate(_playerOneListItemTemplate, _playerOneListItemTemplate.transform.parent);
                }
                else
                {
                    instance = Instantiate(_playerTwoListItemTemplate, _playerTwoListItemTemplate.transform.parent);
                }
                
                instaceText = instance.GetComponent<ListText>();
                    
                list.Add(dictionaryList.Key,instance);

                if (instaceText != null)
                {
                    instaceText.SetText(dictionaryList.Key.PartName);
                    instaceText.Uncollected();
                    instaceText.EnableObject(true);
                }
            }
            
            _playerList.Add(list);
        }
    }

    public void HandleCollectPart(int playerIndex, SpaceshipPart dictionaryKey, InterationCode interactionCode )
    {
        ListText itemToUpdate = null;
        
        if (_playerList[playerIndex].ContainsKey(dictionaryKey))
        {
            itemToUpdate = _playerList[playerIndex][dictionaryKey].GetComponent<ListText>();
        }
        

        if (itemToUpdate == null)
        {
            Debug.LogError("Item to update is missing");
            return;
        }
            
        switch (interactionCode)
        {
            case InterationCode.Collect:
                itemToUpdate.Collected();
                break;
            case InterationCode.Install:
                itemToUpdate.Installed();
                break;
            case InterationCode.Remove:
                itemToUpdate.Uncollected();
                break;
        }
    }
}

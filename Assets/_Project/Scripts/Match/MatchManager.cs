﻿using System.Collections.Generic;
using UnityEngine;

public class MatchManager : MonoBehaviour
{
    public delegate void UpdateListHandler(int playerIndex, SpaceshipPart dictionaryKey, InterationCode interactionCode);
    public delegate void RefreshListHandler(int playerIndex);
    public delegate bool IsPartNeededHandler(SpaceshipPart part, int index);
    public delegate bool IsAllPartCollectedHandler(int playerIndex);
    public delegate void ShipReadyToFlightHandler(Winner winner);
    public delegate void MatchBeginHandler();
    public delegate Dictionary<SpaceshipPart, bool> GetCheckListHandler(int listIndex);

    public event RefreshListHandler OnRefreshCheckList;
    public event GetCheckListHandler OnGetCheckList;
    public event MatchBeginHandler OnBegin;
    public event UpdateListHandler OnCollectPart;
    public event UpdateListHandler OnDropPart;
    public event IsPartNeededHandler IsPartNeeded;
    public event IsAllPartCollectedHandler IsAllPartCollected;
    public event ShipReadyToFlightHandler OnShipReadyToFlight;

    [SerializeField] private CheckListGenerator _checkListGenerator;
    [SerializeField] private MatchController _matchController;
    [SerializeField] private UIController _uiController;
    
    private void Awake()
    {
        OnRefreshCheckList += _matchController.HandleDropCollectedItens;
        OnDropPart  += _uiController.HandleCollectPart;
        OnGetCheckList += _matchController.HandleGetCheckList;
        OnCollectPart += _uiController.HandleCollectPart;
        OnCollectPart += _matchController.HandlePartCollected;
        IsAllPartCollected += _matchController.HandleIsAllPartCollected;
        IsPartNeeded += _matchController.HandleIsPartNeeded;
        OnShipReadyToFlight += _matchController.HandleMatchFinish;
        _matchController.OnPlayerCanMove += HandlePlayerCanMove;
        
        _matchController.OnSetup += _checkListGenerator.RandomizePartChecklist;
        _matchController.GetAllCollectableItems += _checkListGenerator.HandleGetAllCollectablesItems;
        _matchController.GetCheckList += _checkListGenerator.HandleGeneratePlayerList;
        
        _matchController.OnBegin += _uiController.HandleMatchBegin;
        _matchController.OnTimeUpdate += _uiController.HandleMatchTimeUpdate;
        _matchController.OnCountdownUpdate += _uiController.HandleCountdownUpdate;
        
        _matchController.OnCreateListUI += _uiController.CreateList;
       
        _matchController.OnFinish += GameManager.Instance.HandleMatchFinish;
    }

    private void HandlePlayerCanMove()
    {
        OnBegin?.Invoke();
    }

    public void HandleShipReadyToFlight(Winner winner)
    {
        OnShipReadyToFlight?.Invoke(winner);
    }

    public void HandleDropCollectedItens(int listIndex)
    {
        OnRefreshCheckList?.Invoke(listIndex);
    }
    
    public void HandleCollectPart(int playerIndex, SpaceshipPart dictionaryKey, InterationCode interactionCode)
    {
        OnCollectPart?.Invoke(playerIndex,dictionaryKey,interactionCode);
    }
    
    public void HandleDropPart(int playerIndex, SpaceshipPart dictionaryKey, InterationCode interactionCode)
    {
        OnDropPart?.Invoke(playerIndex,dictionaryKey,interactionCode);
    }
    
    public bool HandleIsPartNeeded(SpaceshipPart part, int listIndex)
    {
        if (IsPartNeeded == null)
        {
            Debug.LogError("Call for part needed is empty");
            return false;
        }
        
        return IsPartNeeded.Invoke(part, listIndex);
    }

    public bool HandlesIsAllPartCollected(int listIndex)
    {
        if (IsAllPartCollected == null)
        {
            Debug.LogError("HandlesIsAllPartCollected is empty");
            return false;
        }
        
        return IsAllPartCollected.Invoke(listIndex);
    }

    public Dictionary<SpaceshipPart, bool> HandleGetCheckList(int listIndex)
    {
        return OnGetCheckList?.Invoke(listIndex);
    }
}

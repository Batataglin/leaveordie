﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class MatchController : MonoBehaviour
{
    public delegate void MatchHandler();
    public delegate void MatchFinishHandler(Winner winner);
    public delegate void MatchUITimeHandler(string value);
    public delegate void UIListHandler(List<Dictionary<SpaceshipPart, bool>> partList);
    public delegate void PlayerCanMoveHandler();
    public delegate List<SpaceshipPart> ItemsToBeFixedHandler();
    public delegate SpaceshipPart[] CollectablesHandler();
    
    public event PlayerCanMoveHandler OnPlayerCanMove;
    public event ItemsToBeFixedHandler GetCheckList;
    public event CollectablesHandler GetAllCollectableItems;
    public event MatchHandler OnSetup;
    public event MatchHandler OnBegin;
    public event MatchFinishHandler OnFinish;
    public event MatchUITimeHandler OnTimeUpdate;
    public event MatchUITimeHandler OnCountdownUpdate;
    public event UIListHandler OnCreateListUI;
    
    [Header("Debug Options")] 
    [SerializeField] private bool _debug = true;
    
    [Header("References")]
    [SerializeField] private GameObject[] _players;
    [SerializeField] private GameObject _spaceshipPartPrefab;
    
    [Header("Spawns")]
    [SerializeField] private Transform[] _playersSpawnPoint;
    [SerializeField] private GameObject _itemSpawnParent;
    [SerializeField] private float _spawnArea;
    
    [Header("Match Settings")]
    [SerializeField, Range(1,5)] private float _matchTime;
    [SerializeField] private Transform _sun;

    private bool _startMatchTime;
    private float _currentMatchTime;
    private AudioSource _audioSource;
    private Transform[] m_collectablesSpawnPoint;
    private List<Transform> _spawnPoints = new List<Transform>();
    private List<Dictionary<SpaceshipPart, bool>> _playersCheckList = new List<Dictionary<SpaceshipPart, bool>>();

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();      
        
        m_collectablesSpawnPoint = _itemSpawnParent.GetComponentsInChildren<Transform>();
        for (int i = 0; i < m_collectablesSpawnPoint.Length; i++)
        {
            _spawnPoints.Add(m_collectablesSpawnPoint[i]);
        }
    }

    private void Start()
    {
        StartCoroutine(MatchCoroutine());
    }
    
#if UNITY_EDITOR && true
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            if (_debug)
            {
                foreach (var dictionary in _playersCheckList)
                {
                    foreach (KeyValuePair<SpaceshipPart, bool> kvp in dictionary)
                    {
                        Debug.Log(string.Format("Key = {0}, Value = {1}", kvp.Key, kvp.Value));
                    }
                }
            }
        }
    }
#endif

    private void FixedUpdate()
    {
        if(_startMatchTime)
        {
            _currentMatchTime-= Time.fixedDeltaTime;
        }
    }

    public Dictionary<SpaceshipPart, bool> HandleGetCheckList(int listIndex)
    {
        return _playersCheckList[listIndex];
    }
    
    public bool HandleIsAllPartCollected(int listIndex)
    {
        int checkListCollectedItens = 0;
        
        foreach (var dictionaryParts in _playersCheckList[listIndex])
        {
            if (dictionaryParts.Value == true)
            {
                checkListCollectedItens++;
            }
        }
        return checkListCollectedItens == _playersCheckList[listIndex].Count;
    }

    public bool HandleIsPartNeeded(SpaceshipPart spaceshipPart, int listIndex)
    {
        foreach (var dictionaryParts in _playersCheckList[listIndex])
        {
            if (dictionaryParts.Key == spaceshipPart)
            {
                return true;
            }
        }
        
        return false;
    }

    public void HandleDropCollectedItens(int listIndex)
    {
        var tempDictionary = _playersCheckList[listIndex];
        
        foreach (var dictionary in tempDictionary.Keys.ToList())
        {
            tempDictionary[dictionary] = false;
        }

        _playersCheckList[listIndex] = tempDictionary;
    }
    
    public void HandlePartCollected(int playerIndex, SpaceshipPart dictionaryKey, InterationCode interactionCode)
    {
        _playersCheckList[playerIndex][dictionaryKey] = true;
    }

    public void HandleMatchFinish(Winner winner)
    {
        OnFinish?.Invoke(winner);
    }
    
    private IEnumerator MatchCoroutine()
    {
        yield return MatchSetupCoroutine();
        
        yield return MatchBeginCoroutine();

        float matchTime = 60 * _matchTime;
        _currentMatchTime = matchTime;
        _startMatchTime = true;
        
        _sun.DOScale(6f,matchTime);
        
        while (_currentMatchTime > 0f)
        {
            _sun.Rotate(Vector3.forward,2f * Time.fixedDeltaTime);
            ConvertMatchTime(_currentMatchTime);

            yield return null;
        }
        
        HandleMatchFinish(Winner.None);
    }
    
    private IEnumerator MatchBeginCoroutine()
    {
        GameManager.Instance.GameState = GameStatus.INGAME;
        PlayerGameObjectActive(true);

        float countdownTime = 5f;
         
        while (countdownTime > 0f)
        {
            countdownTime -= Time.fixedDeltaTime;

            OnCountdownUpdate?.Invoke(string.Format("{0:0}", countdownTime ));
            
            yield return null;
        }
        
        OnPlayerCanMove?.Invoke();
        
        OnBegin?.Invoke();

        _audioSource.volume = 0.5f;
        _audioSource.loop = true;
        _audioSource.Play();
    }

    private IEnumerator MatchSetupCoroutine()
    {
        OnSetup?.Invoke();
        
        for (int i = 0; i < _players.Length; i++)
        {
            if (GetCheckList == null)
            {
                yield break;
            }
            
            Dictionary<SpaceshipPart, bool> tempDictionary = new Dictionary<SpaceshipPart, bool>();

            List<SpaceshipPart> spaceshipPartList = GetCheckList.Invoke();
            
            foreach (var spaceshipPart in spaceshipPartList)
            {
                if (!tempDictionary.ContainsKey(spaceshipPart))
                {
                    tempDictionary.Add(spaceshipPart, false);
                }
            }

            _playersCheckList.Add(tempDictionary);
            
            yield return null;
        }
        
        yield return SpawnCollectablePieceOfSpaceship();
        yield return SpawnPlayers();

        OnCreateListUI?.Invoke(_playersCheckList);
        
        PlayerGameObjectActive(false);
    }
    
    private IEnumerator SpawnCollectablePieceOfSpaceship()
    {
        var partsList = GetAllCollectableItems?.Invoke();

        if (partsList == null)
        {
            Debug.LogError("Collectable array is empty");
            yield break;
        }

        foreach (var collectable in partsList)
        {
            int index = Random.Range(0, _spawnPoints.Count);
            Vector2 spawnPoint = _spawnPoints[index].localPosition;
            Vector2 position = spawnPoint + Random.insideUnitCircle * _spawnArea;

            GameObject collectablePart = Instantiate(_spaceshipPartPrefab, position, Quaternion.identity);
            collectablePart.GetComponent<CollectablePart>().Initialize(collectable);

            _spawnPoints.RemoveAt(index);
            yield return null;
        }
    }

    private IEnumerator SpawnPlayers()
    {
        for (int index = 0; index < _players.Length; index++)
        {
            Transform playerSpawn = _playersSpawnPoint[index];

            GameObject playerInstance = Instantiate(_players[index], playerSpawn);
            
            var playerController = playerInstance.GetComponent<PlayerController>();
            playerController.SetPlayerListItemIndex(index);
            
            var playerSpaceship = playerSpawn.GetComponent<Spaceship>();
            playerSpaceship.SetSpaceshipId(playerController.PlayerId);
            
            MultipleCameraTarget.CameraTargets.Add(playerInstance.transform);
            
            playerInstance.SetActive(true);
            yield return null;
        }
    }
   
    private void PlayerGameObjectActive(bool isActive)
    {
        for (int i = 0; i < _players.Length; i++)
        {
            _players[i].SetActive(isActive);
        }
    }
    
    private void ConvertMatchTime(float currentMatchTime)
    {
        int minutes = Mathf.FloorToInt(currentMatchTime / 60F);
        int seconds = Mathf.FloorToInt(currentMatchTime - minutes * 60);
        string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);

        OnTimeUpdate?.Invoke(niceTime);
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheckListGenerator : MonoBehaviour
{
    [Tooltip("This is the quantity of itens to be randomized. First item is common between players + itens in list")] 
    [SerializeField] private int _listLength = 5;
    [SerializeField] private SpaceshipPart[] m_spaceshipParts;

    private List<SpaceshipPart> _spaceshipParts = new List<SpaceshipPart>();
    private List<SpaceshipPart> _drawnPart = new List<SpaceshipPart>();

    public void RandomizePartChecklist()
    {
        for (int i = 0; i < m_spaceshipParts.Length; i++)
        {
            _spaceshipParts.Add(m_spaceshipParts[i]);
        }
        
        for (int i = 0; i < _listLength; i++)
        {
            SpaceshipPart part = _spaceshipParts[Random.Range(0, _spaceshipParts.Count)];

            _drawnPart.Add(part);
            _spaceshipParts.Remove(part);
        }
    }

    public List<SpaceshipPart> HandleGeneratePlayerList()
    {
        List<SpaceshipPart> playerList = new List<SpaceshipPart>(3);
        
        playerList.Add(_drawnPart.First());

        if (_drawnPart.Count > 1)
        {
            for (int i = 0; i < 2; i++)
            {
                int index = Random.Range(1, _drawnPart.Count);

                playerList.Add(_drawnPart[index]);
                _drawnPart.RemoveAt(index);
            }
        }
        else
        {
            Debug.LogError("Drawn parts are empty. If more lists are needed you must setup list length and add more parts into array");
            return null;
        }

        return playerList;
    }

    public SpaceshipPart[] HandleGetAllCollectablesItems()
    {
        return m_spaceshipParts;
    }
}

﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MatchResultController : MonoBehaviour
{
    [SerializeField] private Button _newMatch;
    [SerializeField] private Button _quitMenu;
    [SerializeField] private Button _quitDesktop;
    [SerializeField] private TextMeshProUGUI _winner;

    private void Awake()
    {
        _winner.text = string.Format("{0} managed to escape from the asteroid!",GameManager.Instance.MatchWinner.ToString());
        _newMatch.onClick.AddListener(HandleNewMatchClick);
        _quitMenu.onClick.AddListener(HandleReturnMenuClick);
        _quitDesktop.onClick.AddListener(Application.Quit);
    }

    private void HandleNewMatchClick()
    {
        GameManager.Instance.HandleChangeScene(Scene.VersusGame.ToString());
    }

    private void HandleReturnMenuClick()
    {
        GameManager.Instance.HandleChangeScene(Scene.Main.ToString());
    }
}

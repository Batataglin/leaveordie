﻿using UnityEngine;
using UnityEngine.UI;

public class MainSceneController : MonoBehaviour
{
    [SerializeField] private Button _versusMatch;
    [SerializeField] private Button _quickMatch;
    [SerializeField] private Button _createRomm;
    [SerializeField] private Button _quitGame;

    [SerializeField] private Scene _versusScene;

    [SerializeField] private Toggle _enableJoystick;
    [SerializeField] private PlayerInput _playerOneInputConfig;

    private void Awake()
    {
        _versusMatch.onClick.AddListener(HandleVersusMatchClick);
        _quickMatch.onClick.AddListener(HandleQuickMatchClick);
        _createRomm.onClick.AddListener(HandleCreateRoomClick);
        _quitGame.onClick.AddListener(Application.Quit);
    }

    private void HandleVersusMatchClick()
    {
        _playerOneInputConfig.IsJoystick = _enableJoystick.isOn;
        
        GameManager.Instance.HandleChangeScene(_versusScene.ToString());
    }
    
    private void HandleQuickMatchClick()
    {
        Debug.Log("TODO Call multiplayer functions");
        //TODO Call multiplayer
    }
    
    private void HandleCreateRoomClick()
    {
        Debug.Log("TODO Create room screen");
        //TODO create room screen
    }
}
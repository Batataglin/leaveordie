﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerInput", menuName = "Leave or die/Player input", order = 101)]

public class PlayerInput : ScriptableObject
{
    [SerializeField] private bool _isJoystick;

    [Header("Keybord")]
    [SerializeField] private KeyCode _left;
    [SerializeField] private KeyCode _right;
    [SerializeField] private KeyCode _up;
    [SerializeField] private KeyCode _down;
    [SerializeField] private KeyCode _jetpack;
    [SerializeField] private KeyCode _shoot;
    
    [Header("Joysitck")]
    [SerializeField] private string _horizontal;
    [SerializeField] private string _vertical;
    [SerializeField] private string _joyJetpack;
    [SerializeField] private string _joyShoot;

    public bool IsJoystick
    {
        get => _isJoystick;
        set => _isJoystick = value;
    }

    public KeyCode Left => _left;

    public KeyCode Right => _right;

    public KeyCode Up => _up;

    public KeyCode Down => _down;

    public KeyCode Jetpack => _jetpack;

    public KeyCode Shoot => _shoot;

    public string Horizontal => _horizontal;

    public string Vertical => _vertical;

    public string JoyJetpack => _joyJetpack;

    public string JoyShoot => _joyShoot;
}

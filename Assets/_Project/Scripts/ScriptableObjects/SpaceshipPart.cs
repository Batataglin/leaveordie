﻿using UnityEngine;

[CreateAssetMenu(fileName = "SpaceshipPart", menuName = "Leave or die/Spaceship part", order = 102)]
public class SpaceshipPart : ScriptableObject
{
    [SerializeField] private string _partName;
    [SerializeField] private string _description;
    [SerializeField] private Sprite _partSprite;
    [SerializeField] private float _timeToInteract;
    [SerializeField] private float _timeToInstall;

    private int _partId;

    public int PartId => _partId;

    public string PartName => _partName;

    public string Description => _description;

    public Sprite PartSprite => _partSprite;

    public float TimeToInteract => _timeToInteract;

    public float TimeToInstall => _timeToInstall;
    
    public float TimeToRemove => _timeToInstall * 1.4f;

    private void Awake()
    {
        _partId = Random.Range(11111, 99999);
    }
}

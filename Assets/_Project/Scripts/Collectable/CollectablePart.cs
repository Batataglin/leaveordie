﻿using TMPro;
using UnityEngine;

public class CollectablePart : MonoBehaviour, ICollectable
{
    private readonly float _spriteSliceSize = 1.28f;

    [SerializeField] private GameObject _collectableNameFeedback;
    [SerializeField] private TextMeshProUGUI _partName;
    
    private int _partId;
    private Coroutine _collecting;
    private bool _isBeginCollected;
    private SpaceshipPart _spaceshipPart;
    
    public int PartId => _partId;

    public SpaceshipPart SpaceshipPart => _spaceshipPart;

    public void Initialize(SpaceshipPart part)
    {
        _spaceshipPart = part;
        gameObject.name = part.PartName;
        _partName.text = part.PartName;
        _partId = part.PartId;
        
        SpriteRenderer spriteRender = GetComponent<SpriteRenderer>();
        spriteRender.sprite = part.PartSprite;
        spriteRender.size = new Vector2(_spriteSliceSize, _spriteSliceSize);
        
        DisplayCollectableName(false);
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<PlayerController>();

        if (player != null)
        {
            DisplayCollectableName(true);
            player.HandleTriggerEnterInteraction(gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var player = other.GetComponent<PlayerController>();

        if (player != null)
        {
            DisplayCollectableName(false);
            
            player.HandleTriggerExitInteraction();
        }
    }

    private void DisplayCollectableName(bool enableDisplay)
    {
        _collectableNameFeedback.SetActive(enableDisplay);
    }
    
    private void Collected()
    {
        Destroy(gameObject);
    }
    

    void ICollectable.Collected()
    {
        Collected();
    }
}
